export module unit;

export import <cstdint>;
export import <string>;
import <ostream>;
import <bitset>;

#include <cassert>


constexpr size_t bit_width(unsigned long values) {
	size_t bits = 0;

	if(!values) return 0;

	while(values > 1) {
		values >>= 1;
		bits++;
	}

	return bits;
}

export struct Instruction {
public:
	enum class Unit : uint8_t {
		ARITHMETIC,
		LOGIC,
		RELATION,
		SHIFT,
		ENDPOINT,
		JUMP,
		SECURITY,
		APPLICATION,
	};

private:
	enum {
		UNIT = 3,
		OPERATION = 8 - UNIT,
	};

public:
	uint8_t unit : UNIT; // class cannot be used as variable name
	uint8_t operation : OPERATION;

public:
	Instruction() = default;
	Instruction(Unit unit, uint8_t operation) : unit(static_cast<uint8_t>(unit)), operation(operation) {};

	static constexpr uint8_t bitmask_operation(const uint8_t bit) {
		assert(bit < OPERATION);
		return 1ul << bit;
	}

	static constexpr uint8_t inverse_operation(const uint8_t operation) {
		assert(bit_width(operation) <= OPERATION);
		return operation ^ (1ul << (OPERATION - 1));
	}

	friend bool operator==(const uint8_t lhs, const Instruction::Unit rhs) {
		return static_cast<Instruction::Unit>(lhs) == rhs;
	}

	friend std::ostream& operator<<(std::ostream& os, const Instruction& rhs) {
		os << "{" << "un=" << std::bitset<UNIT>(rhs.unit) << " op=" << std::bitset<OPERATION>(rhs.operation) << "}";
		return os;
	}
};

export enum class Type : uint8_t {
	INTEGER,
	REAL,
};

export constexpr int operator+(const int& lhs, const Type& rhs) {
	return lhs + static_cast<int>(rhs);
}

export constexpr int operator+(const Type& lhs, const Type& rhs) {
	return static_cast<int>(lhs) + static_cast<int>(rhs);
}

export constexpr int operator*(const int& lhs, const Type& rhs) {
	return lhs * static_cast<int>(rhs);
}

export enum class TypeCase : uint8_t {
	INTEGER2 = 2*Type::INTEGER,
	REAL2 = 2*Type::REAL,
	INTEGER_REAL = Type::INTEGER + Type::REAL,
};

export typedef uint32_t Address;

export struct Control {
private:
	enum class Width : uint8_t {
		ADDRESS = 8*sizeof(Address) - 1,

		TYPE = bit_width(static_cast<int>(Type::REAL)),
		SIGNIFICAND = ADDRESS, // at least hold one address
		EXPONENT = SIGNIFICAND/4,
		REGISTER = TYPE + SIGNIFICAND + EXPONENT,
	};

public:
	enum Size {
		ADDRESS,
		SIGNIFICAND,
		EXPONENT,
	};

public:
	// *_size can enable up to max value bits, where value zero is disabling this functionality
	uint8_t size[3] = {static_cast<uint8_t>(Width::ADDRESS), static_cast<uint8_t>(Width::SIGNIFICAND), static_cast<uint8_t>(Width::EXPONENT)};
	Type type = Type::INTEGER;

	friend std::ostream& operator<<(std::ostream& os, const Control& rhs) {
	os << "{" << "adr_s=" << static_cast<int>(rhs.size[Size::ADDRESS]) << " sgd_s=" << static_cast<int>(rhs.size[Size::SIGNIFICAND]) << " exp_s=" << static_cast<int>(rhs.size[Size::EXPONENT]) << " typ=" << static_cast<int>(rhs.type) << "}";
		return os;
	}
};

export struct Register {
	Control control;
	int32_t significand = 0;
	int8_t exponent = 0;

	Register() = default;
	Register(const Control control, const int32_t significand, const int8_t exponent) : control(control), significand(significand), exponent(exponent) {}

	bool overflow() {
		return significand > (1 << control.size[Control::Size::SIGNIFICAND]);
	}

	friend std::ostream& operator<<(std::ostream& os, const Register& rhs) {
		os << "{" << "sgd=" << rhs.significand << " exp=" << static_cast<int>(rhs.exponent) << " " << rhs.control << "}";
		return os;
	}
};

export union Argument {
	enum class Flag {
		ADDRESS,
		IMMEDIATE,
		REGISTER,
	};
	Address address;
	Register immediate;
};

export std::ostream& operator<<(std::ostream& os, const Argument& rhs) {
	os << "{" << "arg_adr=" << rhs.address  << " arg_imm=" << rhs.immediate << "}";
	return os;
}

export enum class Exception : uint8_t {
	INITIALIZATION = 0x00,

	UNKNOWN_UNIT = 0x01,
	UNKNOWN_OPERATION = 0x02,
	INVALID_ARGUMENT = 0x04,
	INVALID_RESULT = 0x08,
	STALL_EXCEPTION = 0x10,
};

export std::ostream& operator<<(std::ostream& os, const Exception& rhs) {
	os << "{" << "exc=" << static_cast<int>(rhs) << "}";
	return os;
}

export struct Behavior {
	enum class Flag : uint8_t {
		ARGUMENT = 0x01, 	// ADDRESS, IMMEDIATE, REGISTER
		SYMMETRY, 			// SYMMETRIC, ASSYMMETRIC
		SIZE, 				// ADDRESS_SIZE, SIGNIFICAND_SIZE, EXPONENT_SIZE, where first byte of the size is for choosing which size to change
		TYPE,				// INTEGER, REAL
		VECTOR, 			// SCALAR, VECTOR core; @TODO: decide whether this option is necessary; it would be a Signle-Instruction-Multiple-Data (SIMD) implementation: a group of cores get the same instructions, while only differing in their data
	};

	Argument::Flag argument = Argument::Flag::ADDRESS;
	uint16_t symmetry : 1 = false;
	uint16_t vector : 1 = false;
};

export class Unit {
public:
	virtual void execute(const Instruction instruction, Register operand[2], Register& result, Exception& exception) = 0;

	Unit() {}
	virtual ~Unit() {}
};
