export module relation;
import unit;


Register operator>(const Register& lhs, const Register& rhs) {
	switch(static_cast<TypeCase>(lhs.control.type + rhs.control.type)) {
		case TypeCase::INTEGER2: return Register{lhs.control, lhs.significand > rhs.significand, 0};
		case TypeCase::REAL2: return Register{lhs.control, (lhs.exponent == rhs.exponent && lhs.significand > rhs.significand) || lhs.exponent > rhs.exponent, 0};
		case TypeCase::INTEGER_REAL: return Register{{}, 0, 0};
	}
}

Register operator<(const Register& lhs, const Register& rhs) {
	switch(static_cast<TypeCase>(lhs.control.type + rhs.control.type)) {
		case TypeCase::INTEGER2: return Register{lhs.control, lhs.significand < rhs.significand, 0};
		case TypeCase::REAL2: return Register{lhs.control, (lhs.exponent == rhs.exponent && lhs.significand < rhs.significand) || lhs.exponent < rhs.exponent, 0};
		case TypeCase::INTEGER_REAL: return Register{{}, 0, 0};
	}
}

Register operator==(const Register& lhs, const Register& rhs) {
	switch(static_cast<TypeCase>(lhs.control.type + rhs.control.type)) {
		case TypeCase::INTEGER2: return Register{lhs.control, lhs.significand == rhs.significand, 0};
		case TypeCase::REAL2: return Register{lhs.control, lhs.significand == rhs.significand && lhs.exponent == rhs.exponent, 0};
		case TypeCase::INTEGER_REAL: return Register{{}, 0, 0};
	}
}

Register operator!=(const Register& lhs, const Register& rhs) {
	switch(static_cast<TypeCase>(lhs.control.type + rhs.control.type)) {
		case TypeCase::INTEGER2: return Register{lhs.control, lhs.significand != rhs.significand, 0};
		case TypeCase::REAL2: return Register{lhs.control, lhs.significand != rhs.significand || lhs.exponent != rhs.exponent, 0};
		case TypeCase::INTEGER_REAL: return Register{{}, 0, 0};
	}
}

export class Relation : public Unit {
public:
	enum Operation {
		GREATER = Instruction::bitmask_operation(0),
		LESSER = Instruction::inverse_operation(GREATER),
		EQUAL = Instruction::bitmask_operation(1),
		INEQUAL = Instruction::inverse_operation(EQUAL),
	};

	void execute(const Instruction instruction, Register operand[2], Register& result, Exception& exception) override {
		switch(static_cast<Operation>(instruction.operation)) {
			case Operation::GREATER: result = operand[0] > operand[1]; break;
			case Operation::LESSER: result = operand[0] < operand[1]; break;
			case Operation::EQUAL: result = operand[0] == operand[1]; break;
			case Operation::INEQUAL: result = operand[0] != operand[1]; break;
			default: exception = Exception::UNKNOWN_OPERATION;
		}
	}
};
