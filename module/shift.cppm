export module shift;
import unit;


Register operator>>(const Register& lhs, const Register& rhs) {
	switch(static_cast<TypeCase>(lhs.control.type + rhs.control.type)) {
		case TypeCase::INTEGER2: return Register{lhs.control, lhs.significand >> rhs.significand, lhs.exponent};
		case TypeCase::REAL2: return Register{lhs.control, lhs.significand, static_cast<int8_t>(lhs.exponent >> rhs.significand)};
		case TypeCase::INTEGER_REAL: return Register{{}, 0, 0};
	}
}

Register operator<<(const Register& lhs, const Register& rhs) {
	switch(static_cast<TypeCase>(lhs.control.type + rhs.control.type)) {
		case TypeCase::INTEGER2: return Register{lhs.control, lhs.significand << rhs.significand, lhs.exponent};
		case TypeCase::REAL2: return Register{lhs.control, lhs.significand, static_cast<int8_t>(lhs.exponent << rhs.significand)};
		case TypeCase::INTEGER_REAL: return Register{{}, 0, 0};
	}
}


export class Shift : public Unit {
public:
	enum Operation {
		RIGHT = Instruction::bitmask_operation(0),
		LEFT = Instruction::inverse_operation(RIGHT),
		//CLOCKWISE = Instruction::bitmask_operation(1),
		//COUNTERCLOCKWISE = Instruction::inverse_operation(CLOCKWISE),
	};

	void execute(const Instruction instruction, Register operand[2], Register& result, Exception& exception) override {
		switch(static_cast<Operation>(instruction.operation)) {
			case Operation::RIGHT: result = operand[0] >> operand[1]; break;
			case Operation::LEFT: result = operand[0] << operand[1]; break;
			//case Operation::CLOCKWISE: result = operand[0] << operand[1]; break;
			//case Operation::COUNTERCLOCKWISE: result = operand[0] >> operand[1]; break;
			default: exception = Exception::UNKNOWN_OPERATION;
		}
	}
};
