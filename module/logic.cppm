export module logic;
import unit;


Register operator&(const Register& lhs, const Register& rhs) {
	switch(static_cast<TypeCase>(lhs.control.type + rhs.control.type)) {
		case TypeCase::INTEGER2: return Register{lhs.control, lhs.significand & rhs.significand, lhs.exponent};
		case TypeCase::REAL2: return Register{lhs.control, lhs.significand & rhs.significand, static_cast<int8_t>(lhs.exponent & rhs.exponent)};
		case TypeCase::INTEGER_REAL: return Register{{}, 0, 0};
	}
}

Register operator|(const Register& lhs, const Register& rhs) {
	switch(static_cast<TypeCase>(lhs.control.type + rhs.control.type)) {
		case TypeCase::INTEGER2: return Register{lhs.control, lhs.significand | rhs.significand, lhs.exponent};
		case TypeCase::REAL2: return Register{lhs.control, lhs.significand | rhs.significand, static_cast<int8_t>(lhs.exponent | rhs.exponent)};
		case TypeCase::INTEGER_REAL: return Register{{}, 0, 0};
	}
}

Register operator^(const Register& lhs, const Register& rhs) {
	switch(static_cast<TypeCase>(lhs.control.type + rhs.control.type)) {
		case TypeCase::INTEGER2: return Register{lhs.control, lhs.significand ^ rhs.significand, lhs.exponent};
		case TypeCase::REAL2: return Register{lhs.control, lhs.significand ^ rhs.significand, static_cast<int8_t>(lhs.exponent ^ rhs.exponent)};
		case TypeCase::INTEGER_REAL: return Register{{}, 0, 0};
	}
}

Register operator~(const Register& lhs) {
	switch(lhs.control.type) {
		case Type::INTEGER: return Register{lhs.control, ~lhs.significand, lhs.exponent};
		case Type::REAL: return Register{lhs.control, ~lhs.significand, static_cast<int8_t>(~lhs.exponent)};
	}
}

export class Logic : public Unit {
public:
	enum Operation {
		AND = Instruction::bitmask_operation(0),
		NAND = Instruction::inverse_operation(AND),
		OR = Instruction::bitmask_operation(1),
		NOR = Instruction::inverse_operation(OR),
		XOR = Instruction::bitmask_operation(2),
		NXOR = Instruction::inverse_operation(XOR),
		IDENTITY = Instruction::bitmask_operation(3),
		NEGATE = Instruction::inverse_operation(IDENTITY),
	};

	void execute(const Instruction instruction, Register operand[2], Register& result, Exception& exception) override {
		switch(static_cast<Operation>(instruction.operation)) {
			case Operation::AND: result = operand[0] & operand[1]; break;
			case Operation::NAND: result = ~(operand[0] & operand[1]); break;
			case Operation::OR: result = operand[0] | operand[1]; break;
			case Operation::NOR: result = ~(operand[0] | operand[1]); break;
			case Operation::XOR: result = operand[0] ^ operand[1]; break;
			case Operation::NXOR: result = ~(operand[0] ^ operand[1]); break;
			case Operation::IDENTITY: result = operand[0]; break;
			case Operation::NEGATE: result = ~operand[0]; break;
			default: exception = Exception::UNKNOWN_OPERATION;
		}
	}
};
