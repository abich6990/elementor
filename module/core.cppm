export module core;

export import unit;
export import arithmetic;
export import logic;
export import relation;
export import shift;
export import jump;
export import endpoint;

import <iostream>;
import <map>;


export class Core : public Unit {
public:
	Core() {
		// @TODO: giving each unit its own register set could ease a feature Out-of-Order-Execution capability implementation; could have both implementation methods, where one is used for low cost systems and the other for High-Performance-Computing
		unit[Instruction::Unit::ARITHMETIC] = new Arithmetic();
		unit[Instruction::Unit::LOGIC] = new Logic();
		unit[Instruction::Unit::RELATION] = new Relation();
		unit[Instruction::Unit::SHIFT] = new Shift();
		unit[Instruction::Unit::ENDPOINT] = new Endpoint(/*memory*/);
		unit[Instruction::Unit::JUMP] = new Jump(/*memory*/);
		//unit[Instruction::Unit::SECURITY] = new Security();
		//unit[Instruction::Unit::APPLICATION] = new Application(); // @TODO: could use this unit for a function table (e.g. system calls)

		// load program into instruction memory; it is an array of instructions and its corresponding arguments: inst0@{arg}, ..., instM@{arg}
		// memory.program.load(); //

		// @TODO: thread initialization
		// ... [this]() {while(true) cycle();}
	}

	~Core() {
		for(const auto& [key, value] : unit) delete value;
	}

	void enable() {
		// @TODO: core/thread enable
	}

	void disable() {
		// @TODO: core/thread disable
	}

	void execute(const Instruction instruction, Register operand[2], Register& result, Exception& exception) {
		this->instruction = instruction;
		this->operand[0] = operand[0];
		this->operand[1] = operand[1];
		this->result = result;
		this->exception = exception;
		cycle();
		operand[0] = this->operand[0];
		operand[1] = this->operand[1];
		result = this->result;
		exception = this->exception;
	}

	friend std::ostream& operator<<(std::ostream& os, const Core& rhs) {
		std::cout << "{" << "inst=" << rhs.instruction
			<< "\nop[0]=" << rhs.operand[0] << "\nop[1]=" << rhs.operand[1] << "\nres=" << rhs.result
			<< "\nexc=" << rhs.exception << "}";
		return os;
	}

private:
	void cycle() {
		if(exception != Exception::STALL_EXCEPTION) {
			/*instruction = memory.program*/; // read new instruction to execute
		}

		unit[static_cast<Instruction::Unit>(instruction.unit)]->execute(instruction, operand, result, exception);
	}

private:
	Instruction instruction = {Instruction::Unit::JUMP, Jump::Operation::ABSOLUTE};
	Register operand[2] = {{}, {}};
	Register result = {};
	Exception exception = Exception::INITIALIZATION;

	std::map<Instruction::Unit, Unit*> unit;
	// @TODO: cache or managed memory variable for this core

	// @TODO: thread variable, because each core is simulated by a thread
	// @TODO: program memory to access instructions; should each core have its own memory (reference or value member variable)?
	// Memory& memory;
};
