import core;

import <iostream>;
import <vector>;


bool operator==(const Register& lhs, const Register& rhs) {
	switch(static_cast<TypeCase>(lhs.control.type + rhs.control.type)) {
		case TypeCase::INTEGER2: return lhs.significand == rhs.significand; // @TODO: is it fine to ignore the exponent for the integer case?
		case TypeCase::REAL2: return lhs.significand == rhs.significand && lhs.exponent == rhs.exponent;
		case TypeCase::INTEGER_REAL: return false; // @TODO: convert one type to the other and then compare
	}
}

#define UUT(name, instruction, operation) \
uut.push_back( \
[]() { \
	const std::string NAME = #name; \
	size_t successful = 0; \
	size_t failed = 0; \
	Instruction::Unit instruction_unit = Instruction::Unit::instruction; \
	using OperationUnderTest = operation::Operation; \
	Register operand[2]; \
	Register result; \
	Exception exception = Exception::INITIALIZATION; \
	Core core; \
	core.disable();

#define UUT_END(name) \
} \
);

#define EXPECT(operation, value, comparision) \
	do {\
		core.execute({instruction_unit, OperationUnderTest::operation}, operand, result, exception); \
		if(!(comparision)) {\
			std::cout << "case " << #operation << "=" << #value << " failed\n" << core << std::endl; \
			failed++; \
		} else { \
			successful++; \
		} \
	} while(0)

#define EXPECT_EQUAL(operation, value) result = {}; EXPECT(operation, value, (result == value))
#define EXPECT_EXCEPTION(operation, value) exception = Exception::INITIALIZATION; EXPECT(operation, value, (exception == value))

auto test() {
	std::vector<std::function<void()>> uut;

UUT(arithmetic, ARITHMETIC, Arithmetic)
	operand[0].significand = 10;
	operand[1].significand = 25;

	std::cout << NAME << ": good cases" << std::endl;
	EXPECT_EQUAL(ADD, (Register{operand[0].control, 35, operand[0].exponent}));
	EXPECT_EQUAL(SUBTRACT, (Register{operand[0].control, -15, operand[0].exponent}));
	EXPECT_EQUAL(MULTIPLY, (Register{operand[0].control, 250, operand[0].exponent}));
	EXPECT_EQUAL(DIVIDE, (Register{operand[0].control, 0, operand[0].exponent}));
	operand[0].significand = 110;
	EXPECT_EQUAL(DIVIDE, (Register{operand[0].control, 4, operand[0].exponent}));

	std::cout << NAME << ": bad cases" << std::endl;
	EXPECT_EXCEPTION(ADD - 1, Exception::UNKNOWN_OPERATION);
	operand[0].significand = INT32_MAX/4;
	operand[1].significand = 5;
	//EXPECT_EXCEPTION(MULTIPLY, Exception::INVALID_RESULT); // @TODO: implement overflow/underflow

	std::cout << NAME << ": successful=" << successful << " failed=" << failed << std::endl;
UUT_END(arithmetic)

UUT(logic, LOGIC, Logic)
	operand[0].significand = 0b00001100;
	operand[1].significand = 0b00000101;

	std::cout << NAME << ": good cases" << std::endl;
	EXPECT_EQUAL(AND, (Register{operand[0].control, 0b00000100, operand[0].exponent}));
	EXPECT_EQUAL(NAND, (Register{operand[0].control, ~0b00000100, operand[0].exponent}));
	EXPECT_EQUAL(OR, (Register{operand[0].control, 0b00001101, operand[0].exponent}));
	EXPECT_EQUAL(NOR, (Register{operand[0].control, ~0b00001101, operand[0].exponent}));
	EXPECT_EQUAL(XOR, (Register{operand[0].control, 0b00001001, operand[0].exponent}));
	EXPECT_EQUAL(NXOR, (Register{operand[0].control, ~0b00001001, operand[0].exponent}));
	EXPECT_EQUAL(IDENTITY, (Register{operand[0].control, 0b00001100, operand[0].exponent}));
	EXPECT_EQUAL(NEGATE, (Register{operand[0].control, ~0b00001100, operand[0].exponent}));

	std::cout << NAME << ": bad cases" << std::endl;
	EXPECT_EXCEPTION(AND + 2, Exception::UNKNOWN_OPERATION);

	std::cout << NAME << ": successful=" << successful << " failed=" << failed << std::endl;
UUT_END(logic)

UUT(relation, RELATION, Relation)
	operand[0].significand = 10;
	operand[1].significand = 25;

	std::cout << NAME << ": good cases" << std::endl;
	EXPECT_EQUAL(GREATER, (Register{operand[0].control, 0, 0}));
	EXPECT_EQUAL(LESSER, (Register{operand[0].control, 1, 0}));
	EXPECT_EQUAL(EQUAL, (Register{operand[0].control, 0, 0}));
	EXPECT_EQUAL(INEQUAL, (Register{operand[0].control, 1, 0}));

	std::cout << NAME << ": bad cases" << std::endl;
	EXPECT_EXCEPTION(GREATER + 4, Exception::UNKNOWN_OPERATION);

	std::cout << NAME << ": successful=" << successful << " failed=" << failed << std::endl;
UUT_END(relation)

UUT(shift, SHIFT, Shift)
	operand[0].significand = 0b00110000;
	operand[1].significand = 2;

	std::cout << NAME << ": good cases" << std::endl;
	EXPECT_EQUAL(RIGHT, (Register{operand[0].control, 0b00001100, operand[0].exponent}));
	EXPECT_EQUAL(LEFT, (Register{operand[0].control, 0b11000000, operand[0].exponent}));
	//EXPECT_EQUAL(CLOCKWISE, (Register{operand[0].control, 0, operand[0].exponent}));
	//EXPECT_EQUAL(COUNTERCLOCKWISE, (Register{operand[0].control, 1, operand[0].exponent}));

	std::cout << NAME << ": bad cases" << std::endl;
	EXPECT_EXCEPTION(RIGHT + 6, Exception::UNKNOWN_OPERATION);

	std::cout << NAME << ": successful=" << successful << " failed=" << failed << std::endl;
UUT_END(shift)

	return uut;
}

int main(int argc, char* argv[]) {
	for(auto unit : test()) unit();
}
