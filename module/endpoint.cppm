export module endpoint;
import unit;
//import memory;


export class Endpoint : public Unit {
public:
	enum Operation {
		WRITE = Instruction::bitmask_operation(0),
		READ = Instruction::inverse_operation(WRITE),
		STORE = Instruction::bitmask_operation(1),
		LOAD = Instruction::inverse_operation(STORE),
		PUSH = Instruction::bitmask_operation(2),
		POP = Instruction::inverse_operation(PUSH),
		FEEDBACK = Instruction::bitmask_operation(3),
		FEEDFORWARD = Instruction::inverse_operation(FEEDBACK),
	};

private:
	void store(const Register& argument, Exception& exception) {
		/*switch(memory.program.peak().unit) {*/
			//case Instruction::Unit::ARITHMETIC:
			//case Instruction::Unit::LOGIC:
			//case Instruction::Unit::RELATION:
			//case Instruction::Unit::SHIFT:
			//case Instruction::Unit::JUMP:
				//switch(static_cast<Behavior>(argument.exponent)) { // @TODO: register control changes
					//case Behavior::Flag::SIZE: operand[0].control.size[argument.significand & 0xff] = static_cast<uint8_t>(argument.significand >> 8); break;
					//case Behavior::Flag::TYPE: operand[0].control.type = argument.significand; break;
					//default: exception = Exception::INVALID_ARGUMENT;
				//} break;
			//case Instruction::Unit::ENDPOINT:
				//switch(static_cast<Behavior>(argument.exponent)) { // @TODO: register control changes
					//case Behavior::Flag::ARGUMENT: behavior.argument = argument.significand; break;
					//case Behavior::Flag::SYMMETRY: behavior.symmetry = argument.significand; break;
					//default: exception = Exception::INVALID_ARGUMENT;
				//} break;
			//case Instruction::Unit::SECURITY:
				//switch(static_cast<Behavior>(argument.exponent)) { // @TODO: register control changes
					//case Behavior::Flag::VECTOR: break;
					//default: exception = Exception::INVALID_ARGUMENT;
				//} break;
			//case Instruction::Unit::APPLICATION:
				//break;
		/*}*/
	}

public:
	void execute(const Instruction instruction, Register operand[2], Register& result, Exception& exception) override {
		switch(static_cast<Operation>(instruction.operation)) {
			case Operation::WRITE: /*memory.data() = (behavior.symmetric) ? address : argument.address;*/ break;
			case Operation::READ: /* operand[0] = () ? memory.data(argument.address) : argument.immediate; if(behavior.symmetric( address = argument.address;*/ break;
			case Operation::STORE: /*if(memory.program.peak().operation == std::set{Operation::WRITE, Operation::READ}) store(argument.immediate, exception); else ;*/ break; // @TODO: peak at the next instruction for polymorphic configuration; e.g. if using WRITE this could be the option of either using an address or an immediate value
			case Operation::LOAD: /* if(a operand[0].control = memory.data(argument.immediate.significand);*/ break; // @TODO: could be used to save the current register settings (possibly into memory) and restore them; this could be useful to store multiple primitive data types, without the need for repetitive STORE operations
			case Operation::PUSH: /* memory.data.stack = result;*/ break;
			case Operation::POP: /* result = memory.data.stack;*/ break;
			case Operation::FEEDBACK: {Register tmp = result; result = operand[0]; operand[0] = tmp;} break;
			case Operation::FEEDFORWARD: {Register tmp = operand[1]; operand[1] = operand[0]; operand[0] = tmp;} break;
			default: exception = Exception::UNKNOWN_OPERATION;
		}
	}

	//Endpoint(const Memory& memory) : memory(memory) {}
	//Endpoint(const Memory &&) = delete;

private:
	//Memory& memory; // memory where memory.data {=, +=} modify the memory location
	//Address address;
	Behavior behavior;
};
