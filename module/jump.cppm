export module jump;
import unit;
//import memory;


export class Jump : public Unit {
public:
	enum Operation {
		ABSOLUTE = Instruction::bitmask_operation(0),
		RELATIVE = Instruction::inverse_operation(ABSOLUTE),
		// @TODO: would a CALL operation for function calls be useful?
	};

	void execute(const Instruction instruction, Register operand[2], Register& result, Exception& exception) override {
		switch(static_cast<Operation>(instruction.operation)) {
			case Operation::ABSOLUTE: /*if(!result.significand) memory.program = memory.program.argument(operand[0]);*/ break;
			case Operation::RELATIVE: /*if(!result.significand) memory.program += memory.program.argument();*/ break;
			default: exception = Exception::UNKNOWN_OPERATION;
		}
	}

	//Jump(const Memory& memory) : memory(memory) {}
	//Jump(const Memory &&) = delete;

private:
	//const Memory& memory; // memory where memory.program {=, +=} modify the instruction pointer
};
