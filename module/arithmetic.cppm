export module arithmetic;
import unit;


// @TODO: assign result.control.*_size depending on the *hs.control.*_size values, which does avoid most overflow or underflow exceptions; e.g. for integer multiplication use result.*.sig*_size = rhs.*.sig*_size + lhs.*.sig*_size, while for addition it is the max(*hs.*.sig*_size) + 1; Probably testing result afterwards for overflow in added bits and if none occured, then truncate result to max(*hs.*.sig*_size)
Register operator+(const Register& lhs, const Register& rhs) {
	switch(static_cast<TypeCase>(lhs.control.type + rhs.control.type)) {
		case TypeCase::INTEGER2: return Register{lhs.control, lhs.significand + rhs.significand, 0};
		case TypeCase::REAL2: return Register{{}, 0, 0};
		case TypeCase::INTEGER_REAL: return Register{{}, 0, 0};
	}
}

Register operator-(const Register& lhs, const Register& rhs) {
	switch(static_cast<TypeCase>(lhs.control.type + rhs.control.type)) {
		case TypeCase::INTEGER2: return Register{lhs.control, lhs.significand - rhs.significand, 0};
		case TypeCase::REAL2: return Register{{}, 0, 0};
		case TypeCase::INTEGER_REAL: return Register{{}, 0, 0};
	}
}

Register operator*(const Register& lhs, const Register& rhs) {
	switch(static_cast<TypeCase>(lhs.control.type + rhs.control.type)) {
		case TypeCase::INTEGER2: return Register{lhs.control, lhs.significand * rhs.significand, 0};
		case TypeCase::REAL2: return Register{lhs.control, lhs.significand * rhs.significand, static_cast<int8_t>(lhs.exponent + rhs.exponent)};
		case TypeCase::INTEGER_REAL: return Register{{}, 0, 0};
	}
}

Register operator/(const Register& lhs, const Register& rhs) {
	switch(static_cast<TypeCase>(lhs.control.type + rhs.control.type)) {
		case TypeCase::INTEGER2: return Register{lhs.control, lhs.significand / rhs.significand, 0};
		case TypeCase::REAL2: return Register{lhs.control, lhs.significand / rhs.significand, static_cast<int8_t>(lhs.exponent - rhs.exponent)};
		case TypeCase::INTEGER_REAL: return Register{{}, 0, 0};
	}
}

export class Arithmetic : public Unit {
public:
	enum Operation {
		ADD = Instruction::bitmask_operation(0),
		SUBTRACT = Instruction::inverse_operation(ADD),
		MULTIPLY = Instruction::bitmask_operation(1),
		DIVIDE = Instruction::inverse_operation(MULTIPLY),
	};

	void execute(const Instruction instruction, Register operand[2], Register& result, Exception& exception) override {
		switch(static_cast<Operation>(instruction.operation)) {
			case Operation::ADD: result = operand[0] + operand[1]; break;
			case Operation::SUBTRACT: result = operand[0] - operand[1]; break;
			case Operation::MULTIPLY: result = operand[0] * operand[1]; break;
			case Operation::DIVIDE: result = operand[0] / operand[1]; break;
			default: exception = Exception::UNKNOWN_OPERATION;
		}
		exception = (exception != Exception::INITIALIZATION || !result.overflow()) ? exception : Exception::INVALID_RESULT;
	}
};
