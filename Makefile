CXX:=clang++
MODULE_PATH=module
CXX_FLAGS_BASIC=$(CXX_FLAGS) -stdlib=libc++
CXX_FLAGS_STEP1=$(CXX_FLAGS_BASIC) -Wall -std=c++2a -fimplicit-modules -fimplicit-module-maps -fmodules -fprebuilt-module-path=${MODULE_PATH} --precompile
CXX_FLAGS_STEP2=$(CXX_FLAGS) -fmodules
CXX_FLAGS_STEP3=$(CXX_FLAGS_BASIC) -Wall -std=c++2a -fimplicit-modules -fimplicit-module-maps -fprebuilt-module-path=${MODULE_PATH}

SOURCES:=unit.cppm core.cppm # files with fixed compile position
SOURCES:=$(foreach source, $(SOURCES), ! -iname $(source))
SOURCES:=$(MODULE_PATH)/unit.cppm $(shell find -iname "*.cppm" $(SOURCES)) $(MODULE_PATH)/core.cppm # not used because the build order of modules is important
MODULES:=$(SOURCES:.cppm=.pcm)
OBJECTS:=$(SOURCES:.cppm=.o)
PROJECT_NAME=elementor
MAIN:=${MODULE_PATH}/$(PROJECT_NAME).cpp

all: $(MODULES) $(OBJECTS)
	$(CXX) $(CXX_FLAGS_STEP3) $(OBJECTS) $(MAIN) -o $(PROJECT_NAME)
	rm ${MODULE_PATH}/*.pcm

run:
	./$(PROJECT_NAME)

clean:
	rm ${MODULE_PATH}/*.o ${MODULE_PATH}/*.pcm || true
	rm $(PROJECT_NAME) || true


%.pcm: %.cppm
	$(CXX) $(CXX_FLAGS_STEP1) $< -o $@

%.o: %.pcm
	$(CXX) $(CXX_FLAGS_STEP2) -c $< -o $@
